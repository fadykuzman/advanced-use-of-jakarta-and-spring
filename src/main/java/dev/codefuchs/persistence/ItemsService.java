package dev.codefuchs.persistence;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ItemsService {

    @Autowired
    private ItemRepository repository;

    public void save(Item item) {
        repository.save(item);
    }

    public List<Item> getAll() {
        return repository.getAll();
    }
}
