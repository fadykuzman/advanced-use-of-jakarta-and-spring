package dev.codefuchs.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("item")
public class ItemsController {
    @Autowired
    private ItemsService service;

    @GetMapping
    public List<Item> getAll() {
        return service.getAll();
    }

    @PostMapping(consumes = "application/json")
    public void save(@RequestBody Item item) {
        service.save(item);
    }
}
