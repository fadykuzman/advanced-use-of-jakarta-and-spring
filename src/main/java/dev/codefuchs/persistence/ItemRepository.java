package dev.codefuchs.persistence;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItemRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(Item item) {
        entityManager.persist(item);
    }

    public List<Item> getAll() {
        return entityManager.createQuery("""
                SELECT i FROM Item i
                """, Item.class).getResultList();
    }
}
