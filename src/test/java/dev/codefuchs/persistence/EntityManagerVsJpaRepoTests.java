package dev.codefuchs.persistence;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class EntityManagerVsJpaRepoTests {

    @Autowired
    private ItemsService service;

    @Test
    void entity_manager() {
        Item item = new Item("bla");
        service.save(item);

    }
}
